# Laboratorium 5 - Bazy danych i XML w Java'ie
Projekt bazuje na interfejsie **JDBC** oraz korzysta z **SQLite** do obsługi bazy danych. Dodatkowo wykorzystano JavaFX do interfejsu aplikacji. Aplikacja pozwala również importować oraz eksportować dane przy pomocy biblioteki **JAXB**.

## Baza danych
Baza danych przechowuje posty oraz komentarze (powiązane relacją jeden do wielu) przez co posiada dwie tabele. Pierwsza z nich to tabela **posts**, druga **comments**.

Tabela posts
```
id, title, content, author
```

Tabela comments
```
id, post_id, content, author
```

## Import i eksport
Import oraz eksport używają pliku **posts.xml**. 