package app;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import models.Comment;
import models.Database;
import models.Post;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class CommentController implements Initializable {
    @FXML private TextField authorInput;
    @FXML private TextArea contentInput;

    private Database db = new Database();
    private Post post = null;

//    private Stage postStage = new Stage();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /*postStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                refreshPost();
            }
        });*/
    }

    public void createComment(ActionEvent event) {
        Node btn = (Node) event.getSource();
        Stage stage = (Stage) btn.getScene().getWindow();

        String author = authorInput.getText(),
                content = contentInput.getText();

        if(!author.equals("") && !content.equals("")) {
            Comment comment = db.createComment(new Comment(null, content, author, post));
            post.comments.add(comment);

            stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        }
    }

    public void setPost(Post post) {
        this.post = post;
    }
}
