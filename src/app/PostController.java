package app;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import models.Comment;
import models.Database;
import models.Post;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class PostController implements Initializable {
    @FXML private Text postTitle;
    @FXML private Text postAuthor;
    @FXML private Text postContent;

    @FXML private TextField titleInput;
    @FXML private TextField authorInput;
    @FXML private TextArea contentInput;

    @FXML private VBox commentsBox;

    private Database db = new Database();
    private Post post = null;

    private Stage postStage = new Stage();
    private Stage commentStage = new Stage();

    private Integer type = 0; // 0 - view, 1 - edit

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        postStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                refreshPost();
            }
        });

        commentStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                refreshPost();
            }
        });
    }

    public void openEditPostView(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../views/postCreateView.fxml"));
            loader.load();

            PostController controller = loader.getController();
            controller.setPost(post, 1);

            Parent root = loader.getRoot();

            postStage.setTitle("Edycja posta");
            postStage.setScene(new Scene(root));
            postStage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void openCreateCommentView(ActionEvent event) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../views/commentCreateView.fxml"));
            loader.load();

            CommentController controller = loader.getController();
            controller.setPost(post);

            Parent root = loader.getRoot();

            commentStage.setTitle("Skomentuj post");
            commentStage.setScene(new Scene(root));
            commentStage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void savePost(ActionEvent event) {
        Node btn = (Node) event.getSource();
        Stage stage = (Stage) btn.getScene().getWindow();

        String title = titleInput.getText(),
                author = authorInput.getText(),
                content = contentInput.getText();

        System.out.println(type);

        if(!title.equals("") && !author.equals("") && !content.equals("")) {
            if(type == 0) {
                db.createPost(new Post(null, title, content, author));
            } else {
                post.title = title;
                post.author = author;
                post.content = content;

                db.savePost(post);
            }

            stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        }
    }

    public void deletePost(ActionEvent event) {
        Node btn = (Node) event.getSource();
        Stage stage = (Stage) btn.getScene().getWindow();

        if(db.deletePost(post)) {
            stage.fireEvent(new WindowEvent(stage, WindowEvent.WINDOW_CLOSE_REQUEST));
        }
    }

    public void setPost(Post post, Integer type) {
        this.post = post;
        this.type = type;

        if(type == 0) { // view
            refreshPost();
        } else if(type == 1) { // edit
            titleInput.setText(post.title);
            authorInput.setText(post.author);
            contentInput.setText(post.content);
        }
    }

    public void refreshPost() {
        postTitle.setText(post.title);
        postAuthor.setText(post.author);
        postContent.setText(post.content);

        commentsBox.getChildren().clear();

        for (Comment c : post.comments) {
            commentsBox.getChildren().add(c.getContentElement());
            commentsBox.getChildren().add(c.getAuthorElement());
        }
    }
}
