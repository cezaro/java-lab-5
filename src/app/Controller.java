package app;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import models.Database;
import models.Post;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {
    @FXML private ListView<Post> postsList;

    private Stage createPostStage = new Stage();
    private Stage postStage = new Stage();

    private Database db = new Database();

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        refreshPosts();

        postsList.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent click) {
                if (click.getClickCount() == 2) {
                    Post currentPost = postsList.getSelectionModel().getSelectedItem();
                    openPostView(currentPost);
                }
            }
        });

        createPostStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                refreshPosts();
            }
        });

        postStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                refreshPosts();
            }
        });
    }

    public void openPostCreateView() {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("../views/postCreateView.fxml"));

            createPostStage.setTitle("Tworzenie posta");
            createPostStage.setScene(new Scene(root));
            createPostStage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void exportData(ActionEvent event) {
        db.exportData();
    }

    public void importData(ActionEvent event) {
        db.importData();
        refreshPosts();
    }

    private void openPostView(Post post) {
        try {
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(getClass().getResource("../views/postView.fxml"));
            loader.load();

            PostController controller = loader.getController();
            controller.setPost(post, 0);

            Parent root = loader.getRoot();
            postStage.setTitle(post.title);
            postStage.setScene(new Scene(root));
            postStage.show();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void refreshPosts() {
        postsList.getItems().clear();
        postsList.getItems().addAll(db.getPosts());
    }
}
