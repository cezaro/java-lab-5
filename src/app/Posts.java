package app;

import models.Post;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;

@XmlRootElement(name = "Posts")
@XmlAccessorType(XmlAccessType.FIELD)
public class Posts {
    @XmlElement(name = "Post")
    public ArrayList<Post> posts = new ArrayList<>();
}
