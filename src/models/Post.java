package models;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;

@XmlRootElement(name="Post")
@XmlAccessorType(XmlAccessType.FIELD)
public class Post {
    @XmlTransient
    public Integer id;
    public String title;
    public String content;
    public String author;

    @XmlElements(@XmlElement(name="Comment"))
    public ArrayList<Comment> comments = new ArrayList<>();

    public Post() {

    }

    public Post(Integer id, String title, String content, String author) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.author = author;
    }

    @Override
    public String toString() {
        return title + " - " + author + " (Komentarzy: " + comments.size() + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return id.equals(post.id);
    }
}
