package models;

import app.Posts;
import org.sqlite.jdbc4.JDBC4PreparedStatement;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;

public class Database {
    private static final String DRIVER = "org.sqlite.JDBC";
    private static final String DB_URL = "jdbc:sqlite:database.db";

    private Connection conn;
    private Statement stat;

    public Database() {
        try {
            Class.forName(this.DRIVER);
        } catch (ClassNotFoundException e) {
            System.err.println("Brak sterownika JDBC");
            e.printStackTrace();
        }

        try {
            conn = DriverManager.getConnection(DB_URL);
            stat = conn.createStatement();

            conn.createStatement().execute("PRAGMA foreign_keys = ON");

        } catch (SQLException e) {
            System.err.println("Problem z otwarciem połączenia");
            e.printStackTrace();
        }
    }

    public ArrayList<Post> getPosts() {
        ArrayList<Post> posts = new ArrayList<Post>();

        try {
            String sql = "SELECT * FROM `posts` LEFT JOIN `comments` ON `comments`.`post_id` = `posts`.`id` ORDER BY `posts`.`id` DESC ";
            ResultSet result = stat.executeQuery(sql);

            System.out.println("[LOG] SQL: " + sql);

            while(result.next())
            {
                Post post = parsePost(result);

                Integer index = -1;
                if((index = posts.indexOf(post)) != -1) {
                    Post arrayPost = posts.get(index);
                    arrayPost.comments.addAll(post.comments);
                } else {
                    posts.add(post);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }

        return posts;
    }

    public Post createPost(Post post) {
        try {
            String sql = "INSERT INTO `posts` VALUES (NULL, ?, ?, ?);";
            PreparedStatement prepStmt = conn.prepareStatement(sql);

            prepStmt.setString(1, post.title);
            prepStmt.setString(2, post.content);
            prepStmt.setString(3, post.author);
            prepStmt.execute();

            System.out.println("[LOG] SQL: " + sql);

            post.id = parseId(prepStmt);
        } catch (SQLException e) {
            System.err.println("Błąd przy zapisywaniu");
            return null;
        }

        return post;
    }

    public Post savePost(Post post) {
        try {
            String sql = "UPDATE `posts` SET `title` = ?, `content` = ?, `author` = ? WHERE `id` = ?";
            PreparedStatement prepStmt = conn.prepareStatement(sql);

            prepStmt.setString(1, post.title);
            prepStmt.setString(2, post.content);
            prepStmt.setString(3, post.author);
            prepStmt.setInt(4, post.id);
            prepStmt.execute();

            System.out.println("[LOG] SQL: " + sql);
        } catch (SQLException e) {
            System.err.println("Błąd przy zapisywaniu");
            return null;
        }

        return post;
    }

    public boolean deletePost(Post post) {
        try {
            String sql = "DELETE FROM `posts` WHERE `id` = ?";
            PreparedStatement prepStmt = conn.prepareStatement(sql);

            prepStmt.setInt(1, post.id);
            prepStmt.execute();

            System.out.println("[LOG] SQL: " + sql);

        } catch (SQLException e) {
            System.err.println("Błąd przy usuwaniu");
            return false;
        }
        return true;
    }

    private Post parsePost(ResultSet result) {
        try {
            Integer id;
            String title, content, author;

            id = result.getInt(1);
            title = result.getString(2);
            content = result.getString(3);
            author = result.getString(4);

            Post post = new Post(id, title, content, author);

            Integer commentId = result.getInt(5);

            if(commentId > 0) {
                String commentContent, commentAuthor;

                commentContent = result.getString(7);
                commentAuthor = result.getString(8);

                post.comments.add(new Comment(commentId, commentContent, commentAuthor, post));
            }

            return post;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Comment createComment(Comment comment) {
        try {
            String sql = "INSERT INTO `comments` VALUES (NULL, ?, ?, ?);";
            PreparedStatement prepStmt = conn.prepareStatement(sql);

            prepStmt.setInt(1, comment.post.id);
            prepStmt.setString(2, comment.content);
            prepStmt.setString(3, comment.author);
            prepStmt.execute();

            System.out.println("[LOG] SQL: " + sql);

            comment.id = parseId(prepStmt);
        } catch (SQLException e) {
            System.err.println("Błąd przy zapisywaniu");
            return null;
        }

        return comment;
    }

    public boolean exportData() {
        try {
            JAXBContext ctx = JAXBContext.newInstance(Posts.class);
            Marshaller marshaller = ctx.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

            Posts posts = new Posts();
            posts.posts.addAll(getPosts());

            marshaller.marshal(posts, new File("posts.xml"));
        } catch (JAXBException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public boolean importData() {
        File file = new File("posts.xml");
        JAXBContext jaxbContext = null;

        try {
            jaxbContext = JAXBContext.newInstance(Posts.class);

            Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            Posts posts = (Posts) jaxbUnmarshaller.unmarshal(file);

            for (Post post : posts.posts) {
                post = createPost(post);

                for(Comment comment : post.comments) {
                    comment.post = post;
                    createComment(comment);
                }
            }
        } catch (JAXBException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private Integer parseId(Statement prepStmt) {
        try (ResultSet generatedKeys = prepStmt.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return generatedKeys.getInt(1);
            } else {
                throw new SQLException("Blad przy pobieraniu");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
}
