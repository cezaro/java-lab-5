package models;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;
import javafx.scene.text.Text;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Comment {
    @XmlTransient
    public Integer id;

    public String content;
    public String author;

    @XmlTransient
    public Post post;

    public Comment() {

    }

    public Comment(Integer id, String content, String author, Post post) {
        this.id = id;
        this.content = content;
        this.author = author;
        this.post = post;
    }

    public Text getContentElement() {
        Text contentText = new Text();

        contentText.setText(content);
        contentText.setWrappingWidth(340);

        return contentText;
    }

    public Text getAuthorElement() {
        Text authorText = new Text();

        authorText.setText(author);
        authorText.setWrappingWidth(340);
        authorText.setFill(Color.rgb(119, 119, 119));

        return authorText;
    }
}
